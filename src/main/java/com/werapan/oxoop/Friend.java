/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

/**
 *
 * @author sarit
 */
public class Friend {
    private int id;
    private String name;
    private int age;
    private String tel;
    private static int lastid=1;
    
    public Friend(String name, int age,String tel) {
        this.id = lastid++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    

    public int getAge() {
        return age;
    }

    public void setAge(int age)throws Exception {
        if(age<0) {
            throw new Exception();
        }

        this.age = age;
    }
    

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    

    public static int getLastid() {
        return lastid;
    }

    public static void setLastid(int lastid) {
        Friend.lastid = lastid;
    }
    

    @Override
    public String toString() {
        return "Friend{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
   
     public static void main(String[] args) {
         try {
             Friend f1 = new Friend("Haechan", 24, "0611451569");
             Friend f2 = new Friend("Jaemin", 24, "0971366497");
             System .out.println(f1.toString());
             System .out.println(f2.toString());
         }catch (Exception ex) {
             System.out.println("Oh!!");
         }
         
     }

}
